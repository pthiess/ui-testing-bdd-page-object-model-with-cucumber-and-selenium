package step_defs;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebElement;
import setup.SetUp;

import static setup.SetUp.*;

public class LoginPageStepDefs {

    @Given("I start the browser {string}")
    public void startTheBrowser(String browserName) {
        SetUp.setupDriverAndPages();
        System.out.println("The browser is: " + browserName);
    }

    @When("I load the login page")
    public void loadLoginPage() {
        loginPage.loadLoginPage();
    }


    @When("The login button can be pressed")
    public void canPressLoginButton() {
        loginPage.loadLoginPage();
        WebElement loginButton = loginPage.getLoginButton();
        loginButton.click();
    }

    @Then("The {string} login failure message is displayed")
    public void loginButtonHasCorrectText(String failureMessage) {
        WebElement loginFailureMessage = loginPage.getLoginFailureMessage();

        Assertions.assertEquals(failureMessage, loginFailureMessage.getText());
    }

    public static void main(String[] args) {
        LoginPageStepDefs loginPageStepDefs = new LoginPageStepDefs();
        loginPageStepDefs.startTheBrowser("Chrome");
        loginPageStepDefs.loadLoginPage();
        loginPageStepDefs.canPressLoginButton();
        loginPageStepDefs.loginButtonHasCorrectText("Epic sadface: Username is required");
    }

}
