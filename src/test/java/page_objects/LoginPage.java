package page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LoginPage {

    private WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public void loadLoginPage() {
        driver.get("https://www.saucedemo.com/");
    }


    public WebElement getLoginButton() {
        WebElement loginButton = driver.findElement(By.cssSelector("#login-button"));
        return loginButton;
    }

    public WebElement getLoginFailureMessage() {
        WebElement loginFailureMessage = driver.findElement(By.cssSelector(".error-message-container"));
        return loginFailureMessage;
    }


}
