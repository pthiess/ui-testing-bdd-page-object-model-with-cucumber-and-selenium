package setup;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import page_objects.LoginPage;

public class SetUp {

    public static WebDriver webDriver;
    public static LoginPage loginPage;

    public static void setupDriverAndPages() {
        WebDriver webDriver = new ChromeDriver();
        loginPage = new LoginPage(webDriver);
    }
}
