Feature: LoginPage


  Scenario: Can request login
    Given I start the browser "chrome"
    And I load the login page
    When The login button can be pressed
    Then The "Epic sadface: Username is required" login failure message is displayed

